package com.yturraldedavid.mantago;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if(status == ConnectionResult.SUCCESS){

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }else{
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, (Activity)getApplicationContext(),10);
            dialog.show();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);

        // Add a marker in Sydney and move the camera
        double Lat = getIntent().getExtras().getDouble("Lat");
        double Lng = getIntent().getExtras().getDouble("Lng");

        Log.e("Latitud", String.valueOf( Lat));
        Log.e("Longitud", String.valueOf( Lng));

        LatLng sydney = new LatLng(Lat, Lng);


        LatLng cancebi = new LatLng(-0.947417, -80.721746);
        mMap.addMarker(new MarkerOptions().position(cancebi).title("Museo Cancebi").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        LatLng centroCultural = new LatLng(-0.941848, -80.730457);
        mMap.addMarker(new MarkerOptions().position(centroCultural).title("Museo Centro Cultural Manta").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        LatLng murcielago = new LatLng(-0.939401, -80.730193);
        mMap.addMarker(new MarkerOptions().position(murcielago).title("Murcielago").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        LatLng playitaMia = new LatLng(-0.950198, -80.709080);
        mMap.addMarker(new MarkerOptions().position(playitaMia).title("Playita Mia").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        LatLng martinica = new LatLng(-0.945886, -80.745150);
        mMap.addMarker(new MarkerOptions().position(martinica).title("Martinica").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        LatLng chamaco = new LatLng(-0.946956,-80.745966);
        mMap.addMarker(new MarkerOptions().position(chamaco).title("Chamaco").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        LatLng elFaro = new LatLng(-0.942170,-80.731306);
        mMap.addMarker(new MarkerOptions().position(elFaro).title("El Faro").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        LatLng mammaRosa = new LatLng(-0.945207,-80.737637);
        mMap.addMarker(new MarkerOptions().position(mammaRosa).title("Mamma Rosa").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        LatLng Krug = new LatLng(-0.945334,-80.743960);
        mMap.addMarker(new MarkerOptions().position(Krug).title("The Krug").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        LatLng tresPlatos = new LatLng(-0.94758,-80.741223);
        mMap.addMarker(new MarkerOptions().position(tresPlatos).title("Los 3 platos").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

        LatLng wildBar = new LatLng(-0.945047, -80.735863);
        mMap.addMarker(new MarkerOptions().position(wildBar).title("Wild Bar").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        LatLng noaBar = new LatLng(-0.945187, -80.731077);
        mMap.addMarker(new MarkerOptions().position(noaBar).title("Noa Bar").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        LatLng molotov = new LatLng(-0.945045, -80.731661);
        mMap.addMarker(new MarkerOptions().position(molotov).title("Molotov").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));
        LatLng magic = new LatLng(-0.945239, -80.737538);
        mMap.addMarker(new MarkerOptions().position(magic).title("MAGIC").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));

        LatLng oroVerde = new LatLng(-0.941512, -80.732736);
        mMap.addMarker(new MarkerOptions().position(oroVerde).title("Hotel Oro Verde").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        LatLng balandra = new LatLng(-0.942486, -80.730536);
        mMap.addMarker(new MarkerOptions().position(balandra).title("Cabañas balandra").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        LatLng mariaIsabel = new LatLng(-0.941807, -80.735112);
        mMap.addMarker(new MarkerOptions().position(mariaIsabel).title("Hotel Boutique Maria Isabel").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        LatLng mantaHost = new LatLng(-0.944329, -80.748977);
        mMap.addMarker(new MarkerOptions().position(mantaHost).title("MantaHost Hotel").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        LatLng spondylus = new LatLng(-0.944149, -80.725515);
        mMap.addMarker(new MarkerOptions().position(spondylus).title("Hotel Perla Spondylus").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        LatLng losAlmendros = new LatLng(-0.947021, -80.731915);
        mMap.addMarker(new MarkerOptions().position(losAlmendros).title("Hotel Los Almendros").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));


        float zoomlevel=12;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,zoomlevel));

    }
}
